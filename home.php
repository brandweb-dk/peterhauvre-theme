<div class="blog-intro">
  <div class="container">
    <div class="row">
      <div class="col-md-10 intro-text">
        <h1>Velkommen til bloggen</h1>
        <p>
          Du sidder måske netop nu og tænker – åhh nej, endnu en SEO blog. Bloggen her kan i en høj grad komme til at omhandle SEO, men den vil i høj grad også omhandle forskellige cases og tests, som jeg finder interessant at dele med jer.
          På min blog vil du primært kunne læse indlæg af undertegnet. Jeg har blogget i flere år på andre hjemmesider, men fremover vil al, der har med SEO, AdWords og forskellige former for cases samt nye kreative tanker inden for online markedsføring blive offentlig gjort på denne blog.

          <br><br>
          De bedste hilsner, <strong>Peter Hauvre.</strong>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <?php if (!have_posts()) : ?>
      <div class="alert alert-warning">
        <?php _e('Sorry, no results were found.', 'sage'); ?>
      </div>
      <?php get_search_form(); ?>
    <?php endif; ?>

    <?php while (have_posts()) : the_post(); ?>
      <div class="col-md-6">
        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
      </div>
    <?php endwhile; ?>

    <?php the_posts_navigation(); ?>
  </div>
</div>
