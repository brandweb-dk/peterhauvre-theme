<div class="global-contact">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p>Jeg sidder klar til at hjælpe dig, uanset dine behov og ønsker.</p>
        <a href="/kontakt/"><strong>Kontakt mig i dag</strong><span class="fa fa-angle-right"></span></a>
      </div>
    </div>
  </div>
</div>

<footer>
  <div class="container content-info">
    <div class="row">
      <div class="col-md-3 footer-col">
        <?php dynamic_sidebar('footer-1'); ?>
      </div>

      <div class="col-md-3 footer-col">
        <?php dynamic_sidebar('footer-2'); ?>
      </div>

      <div class="col-md-2 footer-col">
        <?php dynamic_sidebar('footer-3'); ?>
      </div>

      <div class="col-md-4 hidden-xs">
        <?php dynamic_sidebar('footer-4'); ?>
      </div>
    </div>
  </div>

  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p>
            Copyright &copy; 2016 Peter Hauvre All Rights Reserved.
          </p>
        </div>
      </div>
    </div>
  </div>
</footer>
