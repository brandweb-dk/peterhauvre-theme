  <div class="col-md-8">
    <div class="single-wrap">
      <?php while (have_posts()) : the_post(); ?>
        <article <?php post_class(); ?>>
          <header>
            <?php get_template_part('templates/entry-meta'); ?>
            <h1 class="entry-title"><?php the_title(); ?></h1>
          </header>
          <div class="entry-content">
            <?php the_content(); ?>
          </div>
          <footer>
            <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
          </footer>
          <?php/* comments_template('/templates/comments.php'); */?>
        </article>
      <?php endwhile; ?>
    </div>
  </div>

<div class="col-md-4">
  <div class="sidebar-single">
    <?php dynamic_sidebar('sidebar-blog'); ?>
  </div>
</div>
