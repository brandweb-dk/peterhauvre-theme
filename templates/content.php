<a href="<?php the_permalink(); ?>">
  <article class="post-entry" <?php post_class(); ?>>
    <header>
      <?php echo get_the_post_thumbnail( $post_id, 'full', array( 'class' => 'img-responsive' ) ); ?>
      <div class="post-meta">
        <?php get_template_part('templates/entry-meta'); ?>
        <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      </div>
    </header>

    <div class="entry-summary">
      <?php the_excerpt(); ?>
      <a class="moretag" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">Læs hele artiklen</a>
    </div>
  </article>
</a>
