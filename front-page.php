
<div class="forside-intro">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Din hjælp til at blive set Online</h1>
        <p class="icons">
          <i class="fa fa-eye wow fadeIn" data-wow-duration="1s"><span>Søgemaskineoptimering</span></i>

          <i class="fa fa-align-left wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s"><span>Hjemmesidetekster</span></i>

          <i class="fa fa-line-chart wow fadeIn" data-wow-duration="1s" data-wow-delay="1s"><span>Søgeordsanalyser</span></i>

          <i class="fa fa-pencil-square-o wow fadeIn" data-wow-duration="1s" data-wow-delay="1.5s"><span>Produktbeskrivelser</span></i>
        </p>

        <p>
          Peter Hauvre er din samarbejdspartner, når det gælder om at blive set på internettet. <br> Uanset om du sælger et fysisk produkt eller en service, kan jeg hjælpe dig med at øge din synlighed på Google.
        </p>
      </div>
    </div>
  </div>
</div>


<div class="referencer">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-12">
        <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/r1.png" alt="" />
      </div>

      <div class="col-md-3 col-sm-3 col-xs-12">
        <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/r2.png" alt="" />
      </div>

      <div class="col-md-3 col-sm-3 col-xs-12">
        <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/r3.png" alt="" />
      </div>

      <div class="col-md-3 col-sm-3 col-xs-12">
        <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/r4.png" alt="" />
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row">
    <div class="col-md-12 beskrivelse-content">
      <?php while (have_posts()) : the_post(); ?>
      <?php get_template_part('templates/content', 'page'); ?>
      <?php endwhile; ?>
    </div>
  </div>
</div>

<div class="tilbud">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h1>Få et kanon tilbud på SEO og tekstforfatning</h1>
        <p class="lead">
          Uanset om du er en lille eller en stor virksomhed, er jeg altid parat til at tage en uforpligtende snak og sammen finde den rette løsning til dig.
          <br><br>
          Udfyld kontaktformularen, og jeg kontakter dig hurtigst muligt.
        </p>
      </div>

      <div class="col-md-5 col-md-offset-1">
        <?php echo do_shortcode( '[contact-form-7 id="149" title="Kontaktformular 1"]' ); ?>
      </div>
    </div>
  </div>
</div>
